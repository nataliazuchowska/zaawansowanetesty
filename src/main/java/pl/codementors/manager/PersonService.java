package pl.codementors.manager;

import java.util.List;
import java.util.Optional;

import pl.codementors.model.Person;
import pl.codementors.repository.PersonRepository;

public class PersonService {

  private final PersonRepository personrepository;

  public PersonService(final PersonRepository personrepository) {
    this.personrepository = personrepository;
  }

  public void addNewPerson(final String pesel, final String name) {
    personrepository.save(new Person(pesel, name));
  }

  public Optional<Person> findPersonByName(final String name) {
    final List<Person> personsWithName = personrepository.findByName(name);
    if (personsWithName.isEmpty()) {
      return Optional.empty();
    } else {
      return Optional.of(personsWithName.iterator().next());
    }
  }

  public Optional<Person> findPersonByPesel(final String pesel) {
    return personrepository.findByPesel(pesel);
  }

  public void removePersonsWithName(final String name) {
    personrepository.findByName(name)
        .forEach(personrepository::remove);
  }

  public Optional<Person> findPersonByPeselAndName(final String pesel, final String name){
    return personrepository.findByName(name).stream()
        .filter(item -> name.equals(item.getName()))
        .findFirst();
  }




//  public boolean remove(final String pesel, final String name) {
//    return personrepository.findByName(name)
//        .stream()
//        .filter(item -> pesel.equals(item.getPesel()))
//        .findFirst()
//        .map(personrepository::remove)
//        .orElse(false);
//  }

}
