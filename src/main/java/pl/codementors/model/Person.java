package pl.codementors.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Person {
  private final String pesel;
  private final String name;

  public Person(final String pesel, final String name) {
    this.pesel = pesel;
    this.name = name;
  }

  public String getPesel() {
    return pesel;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }

    if (!(o instanceof Person)) {
      return false;
    }

    final Person person = (Person) o;

    return new EqualsBuilder()
        .append(pesel, person.pesel)
        .append(name, person.name)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder(17, 37)
        .append(pesel)
        .append(name)
        .toHashCode();
  }
}
