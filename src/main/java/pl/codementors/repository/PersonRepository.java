package pl.codementors.repository;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pl.codementors.model.Person;

public class PersonRepository {

  private final List<Person> persons = new ArrayList<>();

  public void save(final Person person) {
    persons.add(person);
  }

  public boolean remove(final Person person) {
    return persons.remove(person);
  }

  public Optional<Person> findByPesel(final String pesel) {
    return persons.stream()
        .filter(item -> item.getPesel().equalsIgnoreCase(pesel))
        .findFirst();
  }

  public List<Person> findByName(final String name) {
    return persons.stream()
        .filter(item -> item.getName().equals(name))
        .collect(toList());
  }
}
