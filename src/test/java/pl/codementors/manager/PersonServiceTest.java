package pl.codementors.manager;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import pl.codementors.model.Person;
import pl.codementors.repository.PersonRepository;

import java.util.Optional;

import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

    private PersonService personService;

    @Mock
    private PersonRepository personRepository;

    @Before
    public void beforeTest() {
        personService = new PersonService(personRepository);
    }


    @Test
    public void shouldFindPersonByPesel() {
        when(personRepository.findByPesel(null)).thenReturn(Optional.empty());

        Optional<Person> personByPesel = personService.findPersonByPesel(null);

        Assertions.assertThat(personByPesel).isEmpty();

       // when(personService.findPersonByPesel("pesel")).thenReturn()

    }

    @Test
    public void shouldFindPersonByPeselWithEmptyString() {

        when(personRepository.findByPesel("")).thenReturn(Optional.empty());

        Optional<Person> personByPesel = personService.findPersonByPesel("");

        Assertions.assertThat(personByPesel).isEmpty();
    }
}
